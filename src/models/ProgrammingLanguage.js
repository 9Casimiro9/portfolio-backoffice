const mongoose = require("mongoose");

const programmingLanguage = new mongoose.Schema(
  {
    language: {
      type: String,
      required: true,
      min: 3,
      max: 64,
    },
  },
  { collection: "programmingLanguages" }
);

module.exports = mongoose.model("ProgrammingLanguage", programmingLanguage);
