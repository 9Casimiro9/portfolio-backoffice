const mongoose = require("mongoose");

const { Schema } = mongoose;
mongoose.Promise = global.Promise;

const userSchema = new Schema({
  first_name: {
    type: String,
    required: true,
    min: 3,
    max: 64,
  },
  last_name: {
    type: String,
    required: true,
    min: 3,
    max: 64,
  },
  email: {
    type: String,
    required: true,
    min: 6,
    max: 255,
  },
  password: {
    type: String,
    required: true,
    min: 8,
    max: 1024,
  },
  date: {
    type: Date,
    default: Date.now,
  },
  permission: {
    type: Number,
    default: 0,
    min: 0,
    max: 3,
  },
  account_verification: {
    token: {
      type: String,
      required: false,
      min: 0,
      max: 29,
    },
    date: {
      type: Date,
      required: false,
      default: undefined,
    },
  },
});

module.exports = mongoose.models.User || mongoose.model("User", userSchema);
