const mongoose = require("mongoose");

const projectSchema = new mongoose.Schema({
  ofProgrammingLanguages: {
    type: Array,
    required: true,
    min: 3,
    max: 64,
  },
  visible: {
    type: Boolean,
    required: true,
    min: 6,
    max: 255,
  },
  description: {
    type: String,
    required: true,
    min: 8,
    max: 1024,
  },
  fullDescription: {
    type: String,
    required: true,
    min: 8,
    max: 1024,
  },
  gitURL: {
    type: String,
    required: true,
    min: 8,
    max: 1024,
  },
  date: {
    type: Date,
    default: Date.now,
  },
  coverPhoto: {
    type: String,
    default: 0,
    min: 0,
    max: 50,
  },
});

module.exports = mongoose.model("Project", projectSchema);
