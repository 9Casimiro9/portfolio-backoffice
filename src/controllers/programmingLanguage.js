const ProgrammingLanguage = require("../models/ProgrammingLanguage");

class ProgrammingLanguageController {
  static async getProgrammingLanguages(req, res) {
    await ProgrammingLanguage.find({}, async (err, data) => {
      if (!err) {
        res.status(200).send(data);
      } else {
        console.log("error");
        res.status(400).send(err);
      }
    });
    return;
  }
  static async getProgrammingLanguageByID(req, res) {
    const id = req.params.id;
    await ProgrammingLanguage.findOne(
      { _id: id },
      async (err, data) => {
        if (!err) {
          res.status(200).send(data);
        } else {
          console.log("error");
          res.status(400).send(err);
        }
      }
    );
    return;
  }
}

module.exports = ProgrammingLanguageController;
