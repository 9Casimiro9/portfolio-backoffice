const Project = require("../models/Project");

class ProjectController {
  static async getProjects(req, res) {
    const projectData = await Project.find({}, async (err, data) => {
      if (!err) {
        res.status(200).send(data);
      } else {
        console.log("error");
        res.status(400).send(err);
      }
    });
    return projectData;
  }
  static async getProjectByID(req, res) {
    const id = req.params.id;
    const projectData = await Project.findOne(
      { _id: id },
      async (err, data) => {
        if (!err) {
          res.status(200).send(data);
        } else {
          console.log("error");
          res.status(400).send(err);
        }
      }
    );
    return projectData;
  }
}

module.exports = ProjectController;
