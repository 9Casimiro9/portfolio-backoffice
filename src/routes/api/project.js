const router = require("express").Router();

const ProjectController = require("../../controllers/project.js");

// Sign up
router.get("/", ProjectController.getProjects);

router.get("/:id", ProjectController.getProjectByID);

module.exports = router;
