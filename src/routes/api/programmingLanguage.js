const router = require("express").Router();

const ProgrammingLanguageController = require("../../controllers/programmingLanguage.js");

// Sign up
router.get("/", ProgrammingLanguageController.getProgrammingLanguages);

router.get("/:id", ProgrammingLanguageController.getProgrammingLanguageByID);

module.exports = router;
