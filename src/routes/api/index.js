const router = require("express").Router();

router.use("/project", require("./project"));

router.use("/programming-language", require("./programmingLanguage"));

module.exports = router;
