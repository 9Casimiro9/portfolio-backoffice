const express = require("express");
const app = express();
const dotenv = require("dotenv");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");

dotenv.config();

// Connect to DB
mongoose
  .connect(process.env.DB_CONNECT, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("Connected to DB");
    app.emit("dbStarted");
  });

// Middleware
app.use(express.json());

app.use(cookieParser());

// ejs view engine integration
app.use(express.static("public"));
app.use(bodyParser.urlencoded({ extended: false }));

app.get("/", (req, res) => {
  const acceptLanguages = req.headers["accept-language"].split(/,|;/);
  let languages = [];

  acceptLanguages.forEach((language) => {
    if (language.length === 2) {
      languages.push(language);
    }
  });

  res.redirect("/" + languages[0]);
});

// TODO: create http errors pages

// Route Middlewares
app.use(require("./routes"));

app.get("*", (req, res) => {
  res.status(404).render("pages/errors/404", {
    title: "Sign up",
    message: "not found",
  });
});

// app start
const server = app.listen(process.env.PORT, () => {
  console.log("app is running");
  app.emit("appStarted");
});

//server.close();

// Export our app for testing purposes

module.exports = app;
module.exports.server = server;
